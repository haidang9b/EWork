﻿namespace EW.WebAPI.Models.Models.Profiles
{
    public class UpdateStatusModel
    {
        public bool IsOpenForWork { get; set; }
    }
}
