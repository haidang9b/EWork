﻿namespace EW.WebAPI.Models.Models.Profiles
{
    public class UpdateCoverLetterModel
    {
        public string CoverLetter { get; set; }
    }
}
