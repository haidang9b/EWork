﻿namespace EW.WebAPI.Models.Models.Profiles
{
    public class ContactModel
    {
        public string Address { get; set; } = string.Empty;
        public string Linkedin { get; set; } = string.Empty;
        public string Github { get; set; } = string.Empty;
        public string EmailContact { get; set; } = string.Empty;
        public string PhoneNumber { get; set; } = string.Empty;
        public string Objective { get; set; } = string.Empty;
        public string Skills { get; set; } = string.Empty;
    }
}
