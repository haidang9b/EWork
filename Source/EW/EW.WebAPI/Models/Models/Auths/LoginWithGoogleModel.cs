﻿namespace EW.WebAPI.Models.Models.Auths
{
    public class LoginWithGoogleModel
    {
        public string Email { get; set; }
        public string ImageUrl { get; set; }
        public string FullName { get; set; }
        public string GoogleId { get; set; }
    }
}
