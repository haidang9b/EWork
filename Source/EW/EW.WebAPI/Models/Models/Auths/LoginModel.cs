﻿namespace EW.WebAPI.Models.Models.Auths
{
    public class LoginModel
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }
}
