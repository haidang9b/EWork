﻿namespace EW.WebAPI.Models.Models.Auths
{
    public class RecoveryPasswordModel
    {
        public string Email { get; set; }
        public string Username { get; set; }
    }
}
