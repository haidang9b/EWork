﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EW.Commons.Enums
{
    public enum ETeamSize
    {
        ZeroTo50,
        FiftyTo100,
        OneHundredTo200,
        TwoHundredTo300,
        ThreeHundredTo500,
        FiveHundredTo1000,
        OneThousand,
        TwoThousand,
        ThreeThousand,
        FiveThousand,
    }
}
