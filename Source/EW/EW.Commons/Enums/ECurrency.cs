﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EW.Commons.Enums
{
    public enum ECurrency
    {
        [Description("Vietnam Dong")]
        VND = 0,
        [Description("USD")]
        USD
    }
}
