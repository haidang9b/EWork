﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EW.Commons.Enums
{
    public enum EWorkingType
    {
        AtOffice = 1,
        Remote,
        Flexible,
    }
}
