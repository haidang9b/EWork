﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EW.Commons.Enums
{
    public enum EFileType
    {
        CV = 1,
        Report,
        CompanyAvatar
    }
}
