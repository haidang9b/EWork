﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EW.Domain.ViewModels
{
    public class ResponseWriteFile
    {
        public string Path { get; set; }
        public bool IsSuccess { get; set; }
    }
}
