﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EW.Domain.ViewModels
{
    public class ChartResultViewModel
    {
        public string Label { get; set; } = string.Empty;
        public int Value { get; set; }
    }
}
