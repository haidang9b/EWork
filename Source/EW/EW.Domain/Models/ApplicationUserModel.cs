﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EW.Domain.Models
{
    public class ApplicationUserModel
    {
        public string Username { get; set;} = string.Empty;
        public long ApplicationId { get; set; }
    }
}
