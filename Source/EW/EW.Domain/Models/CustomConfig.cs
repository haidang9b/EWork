﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EW.Domain.Models
{
    public class CustomConfig
    {
        public string FrontEndURL { get; set; }
        public string BackEndURL { get; set; } 
    }
}
