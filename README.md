# EWork - Digital Work

## Front-end
 - React 18
 - React router v6
 - Material UI
 - Material Icon

## Back-end
 - ASP.NET API (.NET core 6)
 - Entity Framework
 - MySql
